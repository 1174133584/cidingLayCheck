layui.config({
  base: '../../../layui_exts/cidingLayCheck/'
}).extend({
  regionSelect: 'cidingLayCheck'
}).use(['cidingLayCheck'], function(){
  	var cidingLayCheck = layui.cidingLayCheck;

  	var selectConf=[
		{url:'data.json',type:'GET',fieldId:'rdSparefield21',fieldName:'按区县',showFieldName:'rdSparefield4',hideFieldId:'rdId'},
		{url:'data.json',type:'GET',fieldId:'rdSparefield20',fieldName:'按职务',showFieldName:'rdSparefield4',hideFieldId:'rdId'},
		{url:'data.json',type:'GET',fieldId:'rdSparefield2',fieldName:'按界别',showFieldName:'rdSparefield4',hideFieldId:'rdId'},
		{url:'data.json',type:'GET',fieldId:'rdSparefield16',fieldName:'按学历',showFieldName:'rdSparefield4',hideFieldId:'rdId'}
	];

	cidingLayCheck.initCheck('#checkBtn','请选出席人员',selectConf,'','',function(users){
		//这是回调
	});
  
});